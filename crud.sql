-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2015 at 01:14 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `email`, `fullname`) VALUES
(23, '455', '454554'),
(25, 'n_hasan1991@yahoo.com', 'sdfsdf'),
(26, 'n_hasan1991@yahoo.com', 'nazmul'),
(27, 'n_hasan1991@yahoo.com', 'dsfsdf'),
(30, 'n_hasan1991@yahoo.com', 'werfsdfs'),
(34, 'n_hasan1991@yahoo.com', 'sdfsdf'),
(35, '111111@11111111', 'sd2222222222'),
(36, 'na@ym.com', 'hasan'),
(37, 'nfghfg@ym.com', 'hfdgfasan'),
(38, 'newdata', 'newdata'),
(39, 'alldata', 'alldata'),
(40, 'aldssdldata', 'alsdfsdfldata');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
